from pathlib import Path
from collections import namedtuple
from itertools import combinations

import numpy as np
import PIL

from stylegan import Stylegan, save_face, save_latent

CACHE_DIR = "cache"
RESULTS_DIR = Path("results")
RANDOM_SEED = 17


# ################################################################################################
# STEP 1: run this, generate faces and pick the most promising ones
# ################################################################################################
def generate_faces(num_faces, results_dir):
    model = Stylegan(RANDOM_SEED, CACHE_DIR)

    for i in range(num_faces):
        print(i)
        face, latent = model.generate_one_face()
        save_face(face, results_dir / "{}.png".format(i))
        save_latent(latent, results_dir / "{}.npy".format(i))


# ################################################################################################
# STEP 2: ADD the index (part of file name) of the selected images to the below array
# ################################################################################################

SELECTED_IMAGES = [1, 2, 28, 31, 45, 49, 75, 90, 130, 270, 291, 435, 501, 549, 596]

Latent = namedtuple('Latent', ['index', 'value'])

def recreate_latents(model):
    model.reset_random_state()
    # re-create all latents up to max index
    latents = [Latent(i, model.sample_latent()) for i in range(max(SELECTED_IMAGES)+1)]
    # only keep the actually selected ones
    latents = [latent for latent in latents if latent.index in SELECTED_IMAGES]
    return latents

# ################################################################################################
# STEP 3: use the functions below to create variations / mixings to the selected images
# ################################################################################################


def interpolate_two_faces(num_steps, results_dir):
    model = Stylegan(RANDOM_SEED, CACHE_DIR)

    latents = recreate_latents(model)
    latents_shifted = latents[1:] + latents[:1]

    for latent_a, latent_b in zip(latents, latents_shifted):
        print(latent_a.index, latent_b.index)
        for i, alpha in enumerate(np.linspace(start=0.0, stop=1.0, num=num_steps)):
            latent = alpha * latent_a.value + (1.0-alpha) * latent_b.value
            face, _ = model.generate_one_face(latent)
            save_face(face, results_dir / "{}_{}_{}.png".format(latent_a.index, latent_b.index, i))


# just let the same face be generated multiple times to see noise effect
def noise_variations(num_variations, results_dir):
    model = Stylegan(RANDOM_SEED, CACHE_DIR)

    latents = recreate_latents(model)

    for latent in latents:
        print(latent.index)

        variations = [model.generate_one_face(latent.value)[0] for v in range(num_variations)]
        for v, variation in enumerate(variations):
            save_face(variation, results_dir / "{}_{}.png".format(latent.index, v))

        # from https://github.com/NVlabs/stylegan/blob/master/generate_figures.py
        diff = np.std(np.mean(variations, axis=3), axis=0) * 4
        diff = np.clip(diff + 0.5, 0, 255).astype(np.uint8)
        save_face(diff, results_dir / "{}_diff.png".format(latent.index))


# mix styles of two latents
def style_mixing(results_dir):
    model = Stylegan(RANDOM_SEED, CACHE_DIR)

    latents = recreate_latents(model)

    for latent_a, latent_b in combinations(latents, 2):
        print(latent_a.index, latent_b.index)

        mapped_a = model.run_mapping_only(latent_a.value)
        mapped_b = model.run_mapping_only(latent_b.value)

        face_a = model.run_synthesis_only(mapped_a)
        face_b = model.run_synthesis_only(mapped_b)

        save_face(face_a, results_dir / "{}.png".format(latent_a.index))
        save_face(face_b, results_dir / "{}.png".format(latent_b.index))

        for i in range(1, 18):
            print(i)

            # am a bit confused how the mixing works, would expect it to be exactly the other way around
            # are the latents sorted from the end first?

            # one way
            mapped_mixed = np.copy(mapped_a)
            mapped_mixed[:-i] = mapped_b[:-i]
            face_mixed = model.run_synthesis_only(mapped_mixed)
            save_face(face_mixed, results_dir / "{}_{}_{}.png".format(latent_b.index, latent_a.index, i))

            # and the other
            mapped_mixed = np.copy(mapped_b)
            mapped_mixed[:-i] = mapped_a[:-i]
            face_mixed = model.run_synthesis_only(mapped_mixed)
            save_face(face_mixed, results_dir / "{}_{}_{}.png".format(latent_a.index, latent_b.index, i))


if __name__ == "__main__":
    # generate_faces(num_faces=10, results_dir=RESULTS_DIR / "originals")
    # interpolate_two_faces(num_steps=10, results_dir=RESULTS_DIR / "interpolated")
    # noise_variations(num_variations=10, results_dir=RESULTS_DIR / "noise_variations")
    style_mixing(results_dir=RESULTS_DIR / "style_mixing")
