import sys
sys.path.append("../../stylegan")

import pickle
from pathlib import Path

import numpy as np
import PIL.Image

import dnnlib
import dnnlib.tflib as tflib


class Stylegan:

    def __init__(self, random_seed, cache_dir):
        self.generator = self._load_model(cache_dir)
        self.random_seed = random_seed
        self.reset_random_state()

    def sample_latent(self):
        latents = self.rnd.randn(1, self.generator.input_shape[1])
        return latents[0]

    def generate_one_face(self, latent=None):
        if latent is None:
            latent = self.sample_latent()

        latents = np.array([latent])

        fmt = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
        images = self.generator.run(latents, None, truncation_psi=0.7, randomize_noise=True, output_transform=fmt)
        face = images[0]

        return face, latent

    def run_mapping_only(self, latent):
        return self.generator.components.mapping.run(np.array([latent]), None)[0]

    def run_synthesis_only(self, mapped_latent):
        fmt = dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True)
        images = self.generator.components.synthesis.run(np.array([mapped_latent]), randomize_noise=True,
                                                         truncation_psi=0.7, output_transform=fmt)
        return images[0]

    def reset_random_state(self):
        self.rnd = np.random.RandomState(self.random_seed)

    @staticmethod
    def _load_model(cache_dir):
        """
        Copied from https://github.com/NVlabs/stylegan/blob/master/pretrained_example.py
        :return:
        """
        # Initialize TensorFlow.
        tflib.init_tf()

        # Load pre-trained network.
        url = 'https://drive.google.com/uc?id=1MEGjdvVpUsu1jB4zrXZN7Y4kBBOzizDQ'  # karras2019stylegan-ffhq-1024x1024.pkl
        with dnnlib.util.open_url(url, cache_dir) as f:
            _G, _D, Gs = pickle.load(f)
            # _G = Instantaneous snapshot of the generator. Mainly useful for resuming a previous training run.
            # _D = Instantaneous snapshot of the discriminator. Mainly useful for resuming a previous training run.
            # Gs = Long-term average of the generator. Yields higher-quality results than the instantaneous snapshot.

        # Print network details.
        # Gs.print_layers()

        return Gs


def save_face(face, location: Path):
    location.parent.mkdir(exist_ok=True, parents=True)
    if len(face.shape) == 2:
        PIL.Image.fromarray(face, 'L').save(str(location))
    else:
        PIL.Image.fromarray(face, 'RGB').save(str(location))

def save_latent(latent, location: Path):
    location.parent.mkdir(exist_ok=True, parents=True)
    np.save(str(location), latent)
